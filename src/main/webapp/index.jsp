<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css" />

        <script type="text/javascript">
            function obtenerSonido(){
                var
            }
        </script>








    </head>
    <body background="resources/fondo3.jpg" style="background-size: cover">

        <div class="container">

            <div class="jumbotron">

                <h1 class="display-4"> <p style="text-align: center">Sonidos del mundo marino</h1>
                <p class="lead" style="text-align: center">Oscar Díaz Cabrera</p>
                <hr class="my-4">

                <br>
                
                <div class="row">
                    
                        
                        <div class="col-md-2 text-center">

                        <div class="card">
                            <img class="card-img-top" src="resources/ballena2.jpg" >
                            <div class="card-body" >

                            </div>

                        </div>
                        <audio id="sonballena" src="resources/sonidoballena.mp3" ></audio>
                        <a class="btn btn-success btn-lg" onclick="alert('ATENCIÓN está sonando una ballena', document.getElementById('sonballena').play())" href="#" role="button">Ballena</a>
                    </div>
                    <div class="col-md-3">
                        
                    </div>
                    
                    <div class="col-md-2 text-center">

                        <div class="card">
                            <img class="card-img-top" src="resources/delfin.jpg" >
                            <div class="card-body" >

                            </div>

                        </div>
                        <audio id="sondelfin" src="resources/sonidodelfin.mp3" ></audio>
                        <a class="btn btn-success btn-lg" onclick="alert('ATENCIÓN está sonando un delfin!', document.getElementById('sondelfin').play())" href="#" role="button">Delfín</a>
                    </div>
                    <div class="col-md-3">
                        
                    </div>
                    <div class="col-md-2 text-center">

                        <div class="card">
                            <img class="card-img-top" src="resources/orca.png" >
                            <div class="card-body" >

                            </div>

                        </div>
                        <audio id="sonorca" src="resources/sonidoorca.mp3" ></audio>
                        <a class="btn btn-success btn-lg" onclick="alert('ATENCIÓN está sonando una orca', document.getElementById('sonorca').play())" href="#" role="button">Orca</a>
                    </div>
              
                </div>
                
                <div class="row">
                    <div class="col-md-2">
                        
                    </div>
               
                    <div class="col-md-2 text-center">

                        <div class="card">
                            <img class="card-img-top" src="resources/pelicano.jpg" >
                            <div class="card-body" >

                            </div>

                        </div>
                        <audio id="sonpelicano" src="resources/sonidopelicano.mp3" ></audio>
                        <a class="btn btn-success btn-lg" onclick="alert('ATENCIÓN está sonando un pelicano', document.getElementById('sonpelicano').play())" href="#" role="button">Pelicano</a>
                    </div>
                    <div class="col-md-4">
                        
                    </div>
           
                    <div class="col-md-2 text-center">

                        <div class="card">
                            <img class="card-img-top" src="resources/lobomarino.jpg" >
                            <div class="card-body" >

                            </div>

                        </div>
                        <audio id="sonlobo" src="resources/sonidolobomarino.mp3" ></audio>
                        <a class="btn btn-success btn-lg" onclick="alert('ATENCIÓN está sonando un lobo marino', document.getElementById('sonlobo').play())" href="#" role="button">Lobo Marino</a>
                    </div>
                    <div class="col-md-2">
                        
                    </div>
        
                </div>

            </div>
        </div>
    </body>
</html>
